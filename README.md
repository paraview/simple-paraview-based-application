# Simple ParaView Based Application

A simple ParaView based application, copied from paraview/Examples/CustomApplications/SimpleParaView.

Intended to be used with ParaView Based Superbuild Example.

# License

Like ParaView, This ParaView based application is distributed under the OSI-approved BSD
3-clause License. See [Copyright.txt][] for details.

[Copyright.txt]: Copyright.txt
